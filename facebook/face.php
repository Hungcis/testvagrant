
<?php
    session_start();
    include './modelface.php';
    require_once '../model.php';

    
    
    $app_id = "332280843802455";
    $app_secret = "89f954a0109b5afd1f38f644955028a2";
    $redirect_uri = urlencode("http://hungproject.herokuapp.com/facebook/face.php");

    $code = $_GET['code'];
    $facebook_access_token_uri = "https://graph.facebook.com/oauth/access_token?client_id=$app_id&redirect_uri=$redirect_uri&client_secret=$app_secret&code=$code";
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $facebook_access_token_uri);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    $response = curl_exec($ch);
    curl_close($ch);

    $access_token = str_replace('access_token=', '', explode("&", $response)[0]);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/me?access_token=$access_token");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    $response = curl_exec($ch);
    curl_close($ch);
    $user = json_decode($response);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://graph.facebook.com/me/friends?access_token=$access_token");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    $response = curl_exec($ch);
    curl_close($ch);
    $friend =json_decode($response);
    
    
    
    $m=new model();
    $idface=$m->getidinsert($_SESSION['name']);
    $f=new modelface();
    $f->set($idface, $user->name, $friend->summary->total_count);  
    header("location:../userindex.php");
?>
