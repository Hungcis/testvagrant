
<?php

session_start();
require_once './model.php';
if (isset($_POST['ok'])) {


    $model = new model();
    $password = $_POST['password'];
    $email = $_POST['email'];

    $roles = $model->getroles($email, $password);
    $fullname = $model->getfullname($email, $password);
    if ($roles == 'no') {
        header("location: login.php?login=false");
    } else if ($roles == '1') {
        $_SESSION['roles'] = 1;
		$_SESSION['name']=$email;
        header("location: admin.php");
    } else {
        $_SESSION['roles'] = 0;
		$_SESSION['name']=$email;
        header("location: userindex.php");
    }
}
