<?php

require_once("DAO/userDAO.php");
require_once("DTO/userDTO.php");
require_once ("./model.php");
//Tạo đối tượng cho DTO user
$objuserDTO = new userDTO();
if (($_POST["txtpassword"] == $_POST["repassword"])) {


    if (!empty($_POST["txtuser"])) {
        $string = $_POST["txtuser"];
        $objuserDTO->username = trim($string);
    } else {
        header("location: adduser.php?add=txtuser");
        exit();
    };


    if (!empty($_POST["txtname"])) {
        $string = $_POST["txtname"];
        $objuserDTO->fullname = trim($string);
    } else {
        header("location: adduser.php?add=txtname");
        exit();
    };
    if (!empty($_POST["txtemail"])) {
        $string = $_POST["txtemail"];
        $objuserDTO->email = trim($string);
    } else {
        header("location: adduser.php?add=txtemail");
        exit();
    };

    if (!empty($_POST["txtpassword"])) {
        $string = $_POST["txtpassword"];
        $objuserDTO->password = trim($string);
    } else {
        header("location: adduser.php?add=txtpassword");
        exit();
    };
    $model = new model();

    if (!$model->checkmailwebformat($_POST["txtemail"])) {
        header("location: adduser.php?add=email not format or");
        exit();
    }

    if ($model->checkin($_POST["txtemail"])) {
        header("location: adduser.php?email=txtemail");
        exit();
    } 
    
        $objuserDAO = new userDAO();
        $objuserDAO->insert($objuserDTO);
        header("Location:listuser.php");
    
} else {
    header("location: adduser.php?add=Password and repassword fail");
}
?>