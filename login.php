<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>UserLogin</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <script src="userlogin.js" type="text/javascript" charset="utf-8" async defer></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </head>
    <body>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        <div class="container">
            <div  class="row">
                <div  class="col-md-12">
                    <h1 class="page-header text-center">Welcome...!</h1>
                </div>
                <div  class="jumbotron col-md-3 center-block">

                    <h3 class="page-header">Please Sign In</h3>
                    <?php
                    if (isset($_GET['login']) && $_GET['login'] == 'false')
                        echo '<h5 style="color: red">Login fail.<br> Email or password wrong</h5>';
                    ?>
                    <form action="controller.php" method="post" name="frmlogin">
                        <input type="text" name="email" id="txtemail" class="form-control" placeholder="Email" required><br>
                        <div style="color:#FF0000" id="emailfail"></div>
                        <input type="password" name="password" id="txtpass" class="form-control" placeholder="Password" required><br>
                        <div style="color:#FF0000" id="passfail"></div>
                        <input type="checkbox" name="" value="">Remember Me
                        <br>
                        <input type="submit" name="ok" value="Login" class="btn btn-primary form-control">
                        <a href="#" class="text-info">Forget to password</a>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>