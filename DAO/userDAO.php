<?php

class userDAO {

    public function insert($userDTO) {
        require_once './config.php';
        $conf = new config();
        $s = $conf->local();
        $con = mysqli_connect($s["server"], $s["username"], $s["password"], $s["new_link"]);
        $strQuery = "insert into users(username,fullname,email,password) values ('$userDTO->username','$userDTO->fullname','$userDTO->email','$userDTO->password')";
        mysqli_query($con, $strQuery);
        mysqli_close($con);
    }

    public function update($userDTO) {

        require_once './config.php';

        $conf = new config();
        $s = $conf->local();
        $con = mysqli_connect($s["server"], $s["username"], $s["password"], $s["new_link"]);
        $strQuery = "update users set fullname='{$userDTO->fullname}', email='{$userDTO->email}',password='{$userDTO->password}',username='{$userDTO->username}'where id={$userDTO->id} ";
        mysqli_query($con, $strQuery) or die(mysqli_errno());
        mysqli_close($con);
    }

    public function deleteById($id) {
        require_once './config.php';

        $conf = new config();
        $s = $conf->local();
        $con = mysqli_connect($s["server"], $s["username"], $s["password"], $s["new_link"]);
        $strQuery = "delete from users where id=$id";
        mysqli_query($con, $strQuery);
        mysqli_close($con);
    }

    public function getAll() {

        require_once './config.php';

        $conf = new config();
        $s = $conf->local();
        $con = mysqli_connect($s["server"], $s["username"], $s["password"], $s["new_link"]);
        $strQuery = "select * from users";
        $result = mysqli_query($con, $strQuery);
        $arrUser = array();
        while ($row = mysqli_fetch_array($result)) {
            $us = new userDTO();
            $us->id = $row["id"];
            $us->username = $row["username"];
            $us->fullname = $row["fullname"];
            $us->email = $row["email"];
            $us->password = $row["password"];
            $arrUser[] = $us;
        }
        mysqli_close($con);
        return $arrUser;
    }

    public function getById($id) {
        require_once './config.php';

        $conf = new config();
        $s = $conf->local();
        $con = mysqli_connect($s["server"], $s["username"], $s["password"], $s["new_link"]);
        $strQuery = "select * from users where id=$id";
        $result = mysqli_query($con, $strQuery);
        $us = new userDTO();
        $row = mysqli_fetch_array($result);
        if ($row!=NULL) {
            $us->id = $row["id"];
            $us->username = $row["username"];
            $us->fullname = $row["fullname"];
            $us->email = $row["email"];
            $us->password = $row["password"];
        }
        mysqli_close($con);
        return $us;
    }

}

?>