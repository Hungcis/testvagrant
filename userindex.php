<?php
if (!isset($session)) {
    session_start();
}


require_once './config.php';
require_once './model.php';
$m = new model();
$id = $m->getidshow($_SESSION['name']);
$conf = new config();
$s = $conf->local();
$con = mysqli_connect($s["server"], $s["username"], $s["password"], $s["new_link"]);
$row = [
    "twittername" => '',
    'twitterfollower' => ''
];
$fa = [
    'nameface' => '',
    'friendface' => ''
];
if (mysqli_num_rows(mysqli_query($con, "select * from twitter where idtwitter='{$id}'")) > 0) {
    $re = mysqli_query($con, "select * from twitter where idtwitter='{$id}'");
    $row = mysqli_fetch_array($re);
}

if (mysqli_num_rows(mysqli_query($con, "select * from face where idface='{$id}'")) > 0) {
    $re = mysqli_query($con, "select * from face where idface='{$id}'");
    $fa = mysqli_fetch_array($re);
}
?>



<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Connecting Success</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </head>
    <body>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <div class="container">
            <?php
            if (isset($_SESSION['name']) && $_SESSION['name']) {
                echo ' <br/><form   class="text-right"> Xin chào: ' . $_SESSION['name'] . '
                   <button >
                      
                      <a href="index.php"> logout</a>
                   </button> 
               </form>';
            } else {
                header("location: index.php");
            }
            if (isset($_SESSION['roles']) && $_SESSION['roles'] != 0) {

                header("location: index.php");
            }
            ?>
            <div  class="row">
                <div  class="col-md-12">
                    <h1 class="page-header">Influencer Marketing</h1>
                </div>
                <div id="menu" class="jumbotron col-md-3">

                    <h3 class="page-header">Menu</h3>
                    <input type="text" name="" value="" class="form-control" placeholder="Search">
                    <button type="button" class="btn btn-default"><span class="glyphicon glyphicon-search" aria-hidden="true"></span>Search</button><br>
                    <a href=""><span class="glyphicon glyphicon-cog" aria-hidden="true"></span>App Install</a>

                </div>
                <div id="content" class="jumbotron col-md-9">
                    <h2 class="page-header">App Install</h2>

                    <div class="col-md-4">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class=" text-primary">Facebook</th>
                                </tr>

                            </thead>
                            <tbody>
                                <tr>
                                    <td>Facebook: <?php echo$fa["nameface"];
            ?></td>

                                </tr>
                                <tr>
                                    <td>Total Friend: <?php echo$fa["friendface"];
            ?></td>

                                </tr>
                                <tr>
                                    <td><a href="https://www.facebook.com/dialog/oauth?client_id=332280843802455&scope=user_friends&redirect_uri=http://hungproject.herokuapp.com/facebook/face.php"> <button type="button" class="form-control btn btn-primary">Connect to Facebook</button> </a></td> 

                                </tr>
                            </tbody>
                        </table>

                    </div>
                    <div class=" col-md-4">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class=" text-primary">Twitter</th>
                                </tr>

                            </thead>
                            <tbody>
                                <tr>
                                    <td>Twitter: <?php echo$row["twittername"];
            ?></td>

                                </tr>
                                <tr>
                                    <td>Total Follower: <?php echo $row["twitterfollower"];
            ?></td>

                                </tr>
                                <tr>
                                    <td><a href="twitter/twitter.php"><button type="button" class="form-control btn btn-info">Connect to Twitter</button></a></td>

                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class=" col-md-4">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th class=" text-primary">Instagram</th>
                                </tr>

                            </thead>
                            <tbody>
                                <tr>
                                    <td>Instagram:</td>

                                </tr>
                                <tr>
                                    <td>Total Folower:</td>

                                </tr>
                                <tr>
                                    <td><button type="button" class="form-control btn btn-warning">Connect to Instagram</button></td>

                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html> 