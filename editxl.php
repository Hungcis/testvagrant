<?php

require_once("DAO/userDAO.php");
require_once("DTO/userDTO.php");
require_once("./model.php");
if (isset($_POST)) {
    $objuserDTO = new userDTO();

    if (!empty($_POST["txtid"])) {
        $string = $_POST["txtid"];
        $objuserDTO->id = trim($string);
    } else {
        header("location: edit.php?id={$objuserDTO->id}&&edit=txtid");
        exit();
    };



    if (!empty($_POST["txtusername"])) {
        $string = $_POST["txtusername"];
        $objuserDTO->username = trim($string);
    } else {
        header("location: edit.php?id={$objuserDTO->id}&&edit=txtusername");
        exit();
    };



    if (!empty($_POST["txtfullname"])) {
        $string = $_POST["txtfullname"];
        $objuserDTO->fullname = trim($string);
    } else {
        header("location: edit.php?id={$objuserDTO->id}&&edit=txtfullname");
        exit();
    };
    if (!empty($_POST["txtemail"])) {
        $string = $_POST["txtemail"];
        $objuserDTO->email = trim($string);
    } else {
        header("location: edit.php?id={$objuserDTO->id}&&edit=txtemail");
        exit();
    };
    if (!empty($_POST["txtpassword"])) {
        $string = $_POST["txtpassword"];
        $objuserDTO->password = trim($string);
    } else {
        header("location: edit.php?id={$objuserDTO->id}&&edit=txtpassword ");
        exit();
    };
    $model = new model();

    if (!$model->checkmailwebformat($_POST["txtemail"])) {
        header("location: edit.php?id={$objuserDTO->id}&&edit=email not format or");
        exit();
    }
    if ($model->checkininsert($_POST["txtemail"])) {
        $objuserDAO = new userDAO();
        $objuserDAO->update($objuserDTO);
        header("Location: listuser.php");
    } else {
        header("Location: edit.php?id={$objuserDTO->id}&&email=fail");
    }
}
?>		